﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ZXing;
using ZXing.QrCode;
using System.IO;

public class QRcodeCreator : MonoBehaviour {

//	public RawImage qrcode;
	private string nam;

	public string lv1;
	public string lv2;
	public string lv3;

	string OriginAlbum = @"C:\inetpub\wwwroot\SAWC"; //針測的資料夾
	public int lv1_b = 0;
	public int lv2_b = 0;
	public int lv3_b = 0;

	public RawImage lv1_r;
	public RawImage lv2_r;
	public RawImage lv3_r;

	// Use this for initialization
	void Start () 
    {
		//設定針測的檔案類型，如不指定可設定*.*
		FileSystemWatcher watch = new FileSystemWatcher(OriginAlbum, "*.mp4");
		//開啟監聽
		watch.EnableRaisingEvents = true;
		//新增時觸發事件
		watch.Created += listen;
    }

	void listen(object sender, FileSystemEventArgs e)
	{
		//nam = e.Name;
		nam = Path.GetFileName(e.Name);
		if (lv3_b == 0 && lv1_b == 2 && lv2_b == 2)
		{
			lv3 = nam;
			lv3_b = 1;
		}
		if (lv2_b == 0 && lv1_b == 2)
		{
			lv2 = nam;
			lv2_b = 1;
		}
		if (lv1_b == 0)
		{
			lv1 = nam;
			lv1_b = 1;
		}
			
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (lv1_b == 1)
		{
			CreateQRCode ("http://118.163.191.153/SAWC/" + lv1,lv1_r);
			lv1_b = 2;
		}
		if (lv2_b == 1)
		{
			CreateQRCode ("http://118.163.191.153/SAWC/" + lv2,lv2_r);
			lv2_b = 2;
		}
		if (lv3_b == 1)
		{
			CreateQRCode ("http://118.163.191.153/SAWC/" + lv3,lv3_r);
			lv3_b = 2;
		}
    }

	public void CreateQRCode(string URL,RawImage pic)
    {
        if (URL == "")
        {
            Debug.Log("No URL");
            return;
        }
		Texture2D encoded = new Texture2D(256, 256);
        encoded.filterMode = FilterMode.Point;
        Color32[] color32 = useEncode(URL, encoded.width, encoded.height);//儲存產生的QR Code
        encoded.SetPixels32(color32);//設定要顯示的圖片像素
        encoded.Apply();//申請顯示圖片
        
		pic.texture = encoded;
    }

    private Color32[] useEncode(string textForEncoding, int width, int height)
    {
        //開始進行編碼動作
        BarcodeWriter writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,//設定格式為QR Code
            Options = new QrCodeEncodingOptions//設定QR Code圖片寬度和高度
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);//將字串寫入，同時回傳轉換後的QR Code
    }
}
