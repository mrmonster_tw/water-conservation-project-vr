﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterflow : MonoBehaviour {



	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.localScale.z >= 2.8f) 
		{
			this.GetComponent<AudioSource> ().enabled = true;
		}
	}
	void OnTriggerStay(Collider other)
	{
		if (other.tag == "water" && transform.localScale.z < 2.8f)
		{
			this.transform.localScale = new Vector3 (1, 1, Mathf.Lerp(this.transform.localScale.z,this.transform.localScale.z+0.1f,0.1f*Time.deltaTime));
		}
	}
}
