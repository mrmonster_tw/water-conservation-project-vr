﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttom : MonoBehaviour {

	public bool push = false;
	public AudioSource hitsound;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	void OnTriggerStay(Collider other)
	{
		if (other.tag == "hammer")
		{
			this.transform.localPosition = new Vector3 (transform.localPosition.x,-0.2f,transform.localPosition.z);
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "hammer")
		{
			if (hitsound.enabled == false)
			{
				hitsound.enabled = true;
				StartCoroutine (hitstop());
			}
			push = true;
			StartCoroutine (bhold());
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "hammer")
		{
			this.transform.localPosition = new Vector3 (transform.localPosition.x,-0.1f,transform.localPosition.z);
		}
	}
	private IEnumerator bhold()
	{
		yield return new WaitForSeconds (0.3f);
		push = false;
	}
	private IEnumerator hitstop()
	{
		yield return new WaitForSeconds (1f);
		hitsound.enabled = false;
	}
}
