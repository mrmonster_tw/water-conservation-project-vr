﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour {

	public Material startmat;
	public Material endmat;
	public Material chgriver_1;
	public Material chgriver_2;
	public Material chgriver_3;
	public Material chgriver_4;

	public GameObject river_1;
	public GameObject river_2;
	public GameObject river_3;

	public GameObject srscp;

	public GameObject word;
	public GameObject timebar;
	public GameObject disc;

	public GameObject right_hand;
	public GameObject left_hand;
	public int check = 0;

	void Start ()
	{
		
	}
	void Update ()
	{
		if (word.activeSelf && check == 0)
		{
			left_hand.GetComponent<ctrl_leftword> ().enabled = false;
			right_hand.SetActive (false);
			this.GetComponent<AudioSource> ().enabled = true;
			check = 1;
		}
		if (timebar.activeSelf && check == 1)
		{
			left_hand.GetComponent<ctrl_leftword> ().enabled = true;
			right_hand.SetActive (true);
			disc.SetActive (true);
			check = 2;
		}

		if (srscp.GetComponent<Main>().score > 3)
		{
			startmat.Lerp (startmat,chgriver_1,0.1f);
		}
		if (srscp.GetComponent<Main>().score > 6)
		{
			chgriver_1.Lerp (chgriver_1,chgriver_2,0.1f);
		}
		if (srscp.GetComponent<Main>().score > 9) 
		{
			chgriver_2.Lerp (chgriver_2, chgriver_3, 0.1f);
		}
		if (srscp.GetComponent<Main>().score > 12)
		{
			chgriver_3.Lerp (chgriver_3,chgriver_4,0.1f);
		}
		if (srscp.GetComponent<Main>().score > 15)
		{
			chgriver_4.Lerp (chgriver_4,endmat,0.1f);
		}
		if (srscp.GetComponent<Main>().score > 18 || srscp.GetComponent<Main>().time <= 0)
		{
			river_1.transform.position = new Vector3 (river_1.transform.position.x,river_1.transform.position.y-0.1f,river_1.transform.position.z);
			river_2.transform.position = new Vector3 (river_2.transform.position.x,river_2.transform.position.y-0.1f,river_2.transform.position.z);
			river_3.transform.position = new Vector3 (river_3.transform.position.x,river_3.transform.position.y-0.1f,river_3.transform.position.z);
		}
	}
}
