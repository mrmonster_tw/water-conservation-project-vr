﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//using HTC.UnityPlugin.Vive;

public class item : MonoBehaviour {

	public Material org_mat;
	public Material rim_mat;
	public bool matchg = false;

	public GameObject hold;
	public GameObject treegm;
	GameObject tree_cn;

	public int tree_bron = 0;
	public GameObject main;
	public int x;

	public GameObject def;
	public GameObject word;
	int check = 0;

	public GameObject parti;

	void Start ()
	{
		
	}
	void Update ()
	{
		if (word.activeSelf && check == 0)
		{
			GetComponent<MeshRenderer> ().material = rim_mat;
			StartCoroutine (matchage());
			check = 1;
		}


		if (main.GetComponent<Main>().score > 9)
		{
			this.GetComponent<FlashingController> ().enabled = true;
		}

		treegm = hold.GetComponent<Hold> ().item;
			
		if (matchg && hold.GetComponent<Hold>().touchthing == false && tree_bron == 0 && treegm.tag == "Tree")
		{
			GameObject parti_cn = Instantiate (parti);
			parti_cn.transform.position = this.transform.position;
			Destroy (parti_cn, 2f);

			tree_cn = Instantiate (treegm);
			tree_cn.transform.SetParent (def.transform);
			tree_cn.transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y+5f, transform.localPosition.z);
			tree_cn.transform.localEulerAngles = new Vector3 (0, 0, 0);
			tree_cn.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
			tree_cn.tag = "Untagged";
			main.GetComponent<Main> ().score = main.GetComponent<Main> ().score+0.1f;
			tree_bron = 1;
		}
		if (tree_cn != null && tree_cn.transform.localScale.x < 4 && tree_cn.name == "Acacia_Low(Clone)(Clone)")
		{
			tree_cn.transform.localScale = new Vector3 (tree_cn.transform.localScale.x + 0.1f, tree_cn.transform.localScale.y + 0.1f, tree_cn.transform.localScale.z + 0.1f);
			this.transform.localScale = new Vector3 (0,0,0);
		}
		if (tree_cn != null && tree_cn.transform.localScale.x >= 4 && tree_cn.name == "Acacia_Low(Clone)(Clone)")
		{
			tree_cn = null;
			Destroy (this.gameObject);
		}

		if (tree_cn != null && tree_cn.transform.localScale.x < 3 && tree_cn.name == "Cherry_Blossoms(Clone)(Clone)")
		{
			tree_cn.transform.localScale = new Vector3 (tree_cn.transform.localScale.x + 0.1f, tree_cn.transform.localScale.y + 0.1f, tree_cn.transform.localScale.z + 0.1f);
			this.transform.localScale = new Vector3 (0,0,0);
		}
		if (tree_cn != null && tree_cn.transform.localScale.x >= 3 && tree_cn.name == "Cherry_Blossoms(Clone)(Clone)")
		{
			tree_cn = null;
			Destroy (this.gameObject);
		}

		if (tree_cn != null && tree_cn.transform.localScale.x < 2 && tree_cn.name == "DouglasFir_Low(Clone)(Clone)")
		{
			tree_cn.transform.localScale = new Vector3 (tree_cn.transform.localScale.x + 0.1f, tree_cn.transform.localScale.y + 0.1f, tree_cn.transform.localScale.z + 0.1f);
			this.transform.localScale = new Vector3 (0,0,0);
		}
		if (tree_cn != null && tree_cn.transform.localScale.x >= 2 && tree_cn.name == "DouglasFir_Low(Clone)(Clone)")
		{
			tree_cn = null;
			Destroy (this.gameObject);
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "ray" && hold.GetComponent<Hold>().touchthing)
		{
			GetComponent<MeshRenderer> ().material = rim_mat;
			matchg = true;
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "ray")
		{
			GetComponent<MeshRenderer> ().material = org_mat;
			matchg = false;
		}
	}
	private IEnumerator matchage()
	{
		yield return new WaitForSeconds (2);
		GetComponent<MeshRenderer> ().material = org_mat;
	}
}
