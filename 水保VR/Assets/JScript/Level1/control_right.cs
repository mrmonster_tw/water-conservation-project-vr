﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//using HTC.UnityPlugin.Vive;

public class control_right : MonoBehaviour {

	private SteamVR_TrackedObject trackObj;//控制器

	private SteamVR_Controller.Device controller
	{
		get
		{
			return SteamVR_Controller.Input((int)trackObj.index);
		}
	}

	public bool trig;

	void Start ()
	{
		trackObj = GetComponent<SteamVR_TrackedObject>();//取得控制器腳本物件
	}
	void Update ()
	{
		if (controller.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = true;
		}
		if (controller.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = false;
		}
	}
}
