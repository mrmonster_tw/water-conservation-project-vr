﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ctrl_leftword_lv2 : MonoBehaviour {

	private SteamVR_TrackedObject trackObj;//控制器

	private SteamVR_Controller.Device controller
	{
		get
		{
			return SteamVR_Controller.Input((int)trackObj.index);
		}
	}

	public GameObject lefthand_word;
	public bool trig;

	// Use this for initialization
	void Start () 
	{
		trackObj = GetComponent<SteamVR_TrackedObject>();//取得控制器腳本物件
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (controller.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = true;
		}
		if (controller.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = false;
		}

		if (controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu)) 
		{ 
			if (lefthand_word.activeSelf) 
			{
				lefthand_word.SetActive (false);
			} 
			else 
			{
				lefthand_word.SetActive (true);
			}
		}
	}
}
