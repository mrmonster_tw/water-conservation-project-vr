﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bug : MonoBehaviour {

	public float speed = 0.1f;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (this.tag == "ants")
		{
			transform.Translate (Vector3.forward*speed*Time.deltaTime,Space.Self);
		}
		if (this.tag == "worm")
		{
			transform.Translate (Vector3.right*speed*Time.deltaTime,Space.Self);
		}
		if (this.tag == "worm_1")
		{
			transform.Translate (Vector3.right*speed*Time.deltaTime,Space.Self);
		}
	}
}
