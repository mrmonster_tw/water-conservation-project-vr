﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//using HTC.UnityPlugin.Vive;
using UnityEngine.UI;

public class main : MonoBehaviour {

	public GameObject waterball_1;
	public GameObject waterball_2;
	public GameObject waterball_3;
	public GameObject waterball_4;

	int ran_num;

	public bool bron = false;

	public GameObject can;

	public int combo = 0;
	public RawImage combo_num1;
	public RawImage combo_num2;
	public RawImage combo_num3;
	public RawImage hit_w;
	public RawImage combo_w;
	private float num1;
	private float num2;
	private float num3;

	public Texture num_0;
	public Texture num_1;
	public Texture num_2;
	public Texture num_3;
	public Texture num_4;
	public Texture num_5;
	public Texture num_6;
	public Texture num_7;
	public Texture num_8;
	public Texture num_9;

	void Start ()
	{
		
	}
	void Update ()
	{
		num1 = combo / 100;
		num2 = Mathf.Floor((combo - num1*100)/10);
		num3 = combo - num1*100 - num2*10;
		number (num1,combo_num1);
		number (num2,combo_num2);
		number (num3,combo_num3);
		if (combo != 0)
		{
			combo_num1.enabled = true;
			combo_num2.enabled = true;
			combo_num3.enabled = true;
			hit_w.enabled = true;
			combo_w.enabled = true;
		}
		if (num1 <= 0)
		{
			combo_num1.enabled = false;
		}
		if (num2 <= 0 && combo_num1.enabled == false)
		{
			combo_num2.enabled = false;
		}

		if (bron) 
		{
			if (can.GetComponent<Main> ().time > 120 && can.GetComponent<Main> ().time <= 150)
			{
				StartCoroutine (time_check(Random.Range(1.5f,2f)));
				ran_wb ();
			}
			if (can.GetComponent<Main> ().time > 30 && can.GetComponent<Main> ().time <= 120)
			{
				StartCoroutine (time_check(Random.Range(1f,1.5f)));
				ran_wb ();
				ran_wb ();
			}
			if (can.GetComponent<Main> ().time > 0 && can.GetComponent<Main> ().time <= 30)
			{
				StartCoroutine (time_check(Random.Range(0.5f,1f)));
				ran_wb ();
				ran_wb ();
				ran_wb ();
			}
		}
	}
	void ran_wb ()
	{
		ran_num = Random.Range (1,5);
		if (ran_num == 1)
		{
			GameObject waterball_cn = Instantiate (waterball_1);
			bron = false;
		}
		if (ran_num == 2)
		{
			GameObject waterball_cn = Instantiate (waterball_2);
			bron = false;
		}
		if (ran_num == 3)
		{
			GameObject waterball_cn = Instantiate (waterball_3);
			bron = false;
		}
		if (ran_num == 4)
		{
			GameObject waterball_cn = Instantiate (waterball_4);
			bron = false;
		}
	}
	private IEnumerator time_check(float xx)
	{
		yield return new WaitForSeconds (xx);
		bron = true;
	}
	public void number (float xx,RawImage zz)
	{
		if (xx == 0)
		{
			zz.texture = num_0;
		}
		if (xx == 1)
		{
			zz.texture = num_1;
		}
		if (xx == 2)
		{
			zz.texture = num_2;
		}
		if (xx == 3)
		{
			zz.texture = num_3;
		}
		if (xx == 4)
		{
			zz.texture = num_4;
		}
		if (xx == 5)
		{
			zz.texture = num_5;
		}
		if (xx == 6)
		{
			zz.texture = num_6;
		}
		if (xx == 7)
		{
			zz.texture = num_7;
		}
		if (xx == 8)
		{
			zz.texture = num_8;
		}
		if (xx == 9)
		{
			zz.texture = num_9;
		}
	}
}
