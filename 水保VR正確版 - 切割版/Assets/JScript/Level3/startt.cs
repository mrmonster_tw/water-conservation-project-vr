﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startt : MonoBehaviour {

	public GameObject timebar;
	public GameObject word;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (timebar.activeSelf)
		{
			this.GetComponent<main> ().enabled = true;
		}
		if (word.activeSelf)
		{
			this.GetComponent<AudioSource> ().enabled = true;
		}
	}
}
