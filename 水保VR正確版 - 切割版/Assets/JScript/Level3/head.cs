﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class head : MonoBehaviour {

	public GameObject left;
	public GameObject right;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	void OnTriggerStay(Collider other)
	{
		if (other.tag == "switchbox_1")
		{
			if (left != null)
			{
				left.GetComponent<ctrl> ().shake = true;
			}
			if (right != null)
			{
				right.GetComponent<ctrl> ().shake = true;
			}
		}
		if (other.tag == "switchbox_2")
		{
			if (left != null)
			{
				left.GetComponent<ctrl> ().shake = true;
			}
			if (right != null)
			{
				right.GetComponent<ctrl> ().shake = true;
			}
		}
		if (other.tag == "switchbox_3")
		{
			if (left != null)
			{
				left.GetComponent<ctrl> ().shake = true;
			}
			if (right != null)
			{
				right.GetComponent<ctrl> ().shake = true;
			}
		}
	}
}
