﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//using HTC.UnityPlugin.Vive;

public class waterball : MonoBehaviour {

	public bool point_1 = false;
	public bool point_2 = false;
	public bool point_3 = false;
	public bool point_4 = false;
	public bool behit;
	private bool check;
	private int check_1;

	public GameObject switchbox_1;
	public GameObject switchbox_2;
	public GameObject switchbox_3;

	public GameObject main;
	Animator ani;

	public Sprite orange;
	public Sprite green;

	public GameObject circal_1;
	public GameObject circal_2;
	public GameObject circal_3;
	public GameObject circal_4;

	public Material mat_1;
	public Material mat_2;
	public GameObject GM;
	public GameObject comboani;

	public GameObject perfect;
	public GameObject miss;

	void Start ()
	{
		switchbox_1 = GameObject.FindGameObjectWithTag ("switchbox_1");
		switchbox_2 = GameObject.FindGameObjectWithTag ("switchbox_2");
		switchbox_3 = GameObject.FindGameObjectWithTag ("switchbox_3");
		main = GameObject.FindGameObjectWithTag ("main");
		GM = GameObject.FindGameObjectWithTag ("GM");
		comboani = GameObject.FindGameObjectWithTag ("comboani");
		ani = GetComponent<Animator> ();
		circal_1 = GameObject.FindGameObjectWithTag ("sp_1");
		circal_2 = GameObject.FindGameObjectWithTag ("sp_2");
		circal_3 = GameObject.FindGameObjectWithTag ("sp_3");
		circal_4 = GameObject.FindGameObjectWithTag ("sp_4");
	}
	void Update ()
	{
		if (main.GetComponent<Main> ().time > 50 && main.GetComponent<Main> ().time <= 100)
		{
			this.GetComponent<Animator> ().speed = 2f;
		}
		if (main.GetComponent<Main> ().time <= 50)
		{
			this.GetComponent<Animator> ().speed = 3f;
		}
		if (switchbox_1.GetComponent<buttom>().push) 
		{
			if (point_1)
			{
				behit = false;
				if (check == false)
				{
					circal_1.GetComponent<SpriteRenderer>().sprite = green;
					switchbox_1.GetComponent<MeshRenderer> ().material = mat_2;
					StartCoroutine (chgmat(circal_1,switchbox_1));

					main.GetComponent<Main> ().score = main.GetComponent<Main> ().score + 0.1f;
					ani.SetBool ("cut",true);
					GM.GetComponent<main> ().combo += 1;

					GameObject perfect_fn = Instantiate (perfect,circal_1.transform);
					Destroy (perfect_fn,1f);

					comboani.GetComponent<Animator> ().SetTrigger ("shake");
					check = true;
				}
			}
		}
		if (switchbox_2.GetComponent<buttom>().push) 
		{
			if (point_2)
			{
				behit = false;
				if (check == false)
				{
					circal_2.GetComponent<SpriteRenderer>().sprite = green;
					switchbox_2.GetComponent<MeshRenderer> ().material = mat_2;
					StartCoroutine (chgmat(circal_2,switchbox_2));

					main.GetComponent<Main> ().score = main.GetComponent<Main> ().score + 0.1f;
					ani.SetBool ("cut",true);
					GM.GetComponent<main> ().combo += 1;

					GameObject perfect_fn = Instantiate (perfect,circal_2.transform);
					Destroy (perfect_fn,1f);

					comboani.GetComponent<Animator> ().SetTrigger ("shake");
					check = true;
				}
			}
			if (point_3)
			{
				behit = false;
				if (check == false)
				{
					circal_3.GetComponent<SpriteRenderer>().sprite = green;
					switchbox_2.GetComponent<MeshRenderer> ().material = mat_2;
					StartCoroutine (chgmat(circal_3,switchbox_2));

					main.GetComponent<Main> ().score = main.GetComponent<Main> ().score + 0.1f;
					ani.SetBool ("cut",true);
					GM.GetComponent<main> ().combo += 1;

					GameObject perfect_fn = Instantiate (perfect,circal_3.transform);
					Destroy (perfect_fn,1f);

					comboani.GetComponent<Animator> ().SetTrigger ("shake");
					check = true;
				}
			}
		}

		if (switchbox_3.GetComponent<buttom>().push) 
		{
			if (point_4)
			{
				behit = false;
				if (check == false)
				{
					circal_4.GetComponent<SpriteRenderer>().sprite = green;
					switchbox_3.GetComponent<MeshRenderer> ().material = mat_2;
					StartCoroutine (chgmat(circal_4,switchbox_3));

					main.GetComponent<Main> ().score = main.GetComponent<Main> ().score + 0.1f;
					ani.SetBool ("cut",true);
					GM.GetComponent<main> ().combo += 1;

					GameObject perfect_fn = Instantiate (perfect,circal_4.transform);
					Destroy (perfect_fn,1f);

					comboani.GetComponent<Animator> ().SetTrigger ("shake");
					check = true;
				}
			}
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "waterbox")
		{
			Destroy (this.gameObject,2f);
		}
	}
	void OnTriggerStay(Collider other)
	{
		if (other.tag == "point_1")
		{
			point_1 = true;
			if (behit == false && check_1 == 0)
			{
				behit = true;
				check_1 = 1;
			}

		}
		if (other.tag == "point_2")
		{
			point_2 = true;
			if (behit == false && check_1 == 0)
			{
				behit = true;
				check_1 = 1;
			}
		}
		if (other.tag == "point_3")
		{
			point_3 = true;
			if (behit == false && check_1 == 0)
			{
				behit = true;
				check_1 = 1;
			}
		}
		if (other.tag == "point_4")
		{
			point_4 = true;
			if (behit == false && check_1 == 0)
			{
				behit = true;
				check_1 = 1;
			}
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "point_1")
		{
			point_1 = false;
			if (point_1 == false && behit)
			{
				GM.GetComponent<main> ().combo = 0;
				GM.GetComponent<main> ().combo_num1.enabled = false;
				GM.GetComponent<main> ().combo_num2.enabled = false;
				GM.GetComponent<main> ().combo_num3.enabled = false;
				GM.GetComponent<main> ().hit_w.enabled = false;
				GM.GetComponent<main> ().combo_w.enabled = false;
				GameObject miss_fn = Instantiate (miss,circal_1.transform);
				Destroy (miss_fn,1f);
			}
		}
		if (other.tag == "point_2")
		{
			point_2 = false;
			if (point_2 == false && behit)
			{
				GM.GetComponent<main> ().combo = 0;
				GM.GetComponent<main> ().combo_num1.enabled = false;
				GM.GetComponent<main> ().combo_num2.enabled = false;
				GM.GetComponent<main> ().combo_num3.enabled = false;
				GM.GetComponent<main> ().hit_w.enabled = false;
				GM.GetComponent<main> ().combo_w.enabled = false;
				GameObject miss_fn = Instantiate (miss,circal_2.transform);
				Destroy (miss_fn,1f);
			}
		}
		if (other.tag == "point_3")
		{
			point_3 = false;
			if (point_2 == false && behit)
			{
				GM.GetComponent<main> ().combo = 0;
				GM.GetComponent<main> ().combo_num1.enabled = false;
				GM.GetComponent<main> ().combo_num2.enabled = false;
				GM.GetComponent<main> ().combo_num3.enabled = false;
				GM.GetComponent<main> ().hit_w.enabled = false;
				GM.GetComponent<main> ().combo_w.enabled = false;
				GameObject miss_fn = Instantiate (miss,circal_3.transform);
				Destroy (miss_fn,1f);
			}
		}
		if (other.tag == "point_4")
		{
			point_4 = false;
			if (point_2 == false && behit)
			{
				GM.GetComponent<main> ().combo = 0;
				GM.GetComponent<main> ().combo_num1.enabled = false;
				GM.GetComponent<main> ().combo_num2.enabled = false;
				GM.GetComponent<main> ().combo_num3.enabled = false;
				GM.GetComponent<main> ().hit_w.enabled = false;
				GM.GetComponent<main> ().combo_w.enabled = false;
				GameObject miss_fn = Instantiate (miss,circal_4.transform);
				Destroy (miss_fn,1f);
			}
		}
	}
	private IEnumerator chgmat(GameObject xx,GameObject zz)
	{
		yield return new WaitForSeconds (0.3f);
		xx.GetComponent<SpriteRenderer>().sprite = orange;
		zz.GetComponent<MeshRenderer> ().material = mat_1;
	}
}
