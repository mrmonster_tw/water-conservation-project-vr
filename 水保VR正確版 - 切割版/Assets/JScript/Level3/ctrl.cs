﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//using HTC.UnityPlugin.Vive;

public class ctrl : MonoBehaviour {

	private SteamVR_TrackedObject trackObj;//控制器

	private SteamVR_Controller.Device controller
	{
		get
		{
			return SteamVR_Controller.Input((int)trackObj.index);
		}
	}

	public bool trig;
	public bool shake;

	void Start ()
	{
		trackObj = GetComponent<SteamVR_TrackedObject>();//取得控制器腳本物件
	}
	void Update ()
	{
		if (shake)
		{
			controller.TriggerHapticPulse(50000);
			StartCoroutine (shaketime());
		}

		if (controller.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = true;
		}
		if (controller.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = false;
		}
	}
	private IEnumerator shaketime()
	{
		yield return new WaitForSeconds (0.1f);
		shake = false;
	}
}
