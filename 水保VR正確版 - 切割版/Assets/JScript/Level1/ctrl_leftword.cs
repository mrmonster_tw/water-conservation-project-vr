﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ctrl_leftword : MonoBehaviour {

	private SteamVR_TrackedObject trackObj;//控制器

	private SteamVR_Controller.Device controller
	{
		get
		{
			return SteamVR_Controller.Input((int)trackObj.index);
		}
	}

	public GameObject word;

	public bool trig;

	public GameObject treepos_1;
	public GameObject treepos_2;
	public GameObject treepos_3;
	public GameObject treepos_4;

	public GameObject treeslot_1;
	public GameObject treeslot_2;
	public GameObject treeslot_3;
	public GameObject treeslot_4;

	public GameObject tree_1;
	public GameObject tree_2;
	public GameObject tree_3;
	public GameObject tree_4;

	GameObject tree_cn;

	// Use this for initialization
	void Start () 
	{
		trackObj = GetComponent<SteamVR_TrackedObject>();//取得控制器腳本物件
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (controller.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = true;
		}
		if (controller.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = false;
		}

		if (controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu)) 
		{ 
			if (word.activeSelf) 
			{
				word.SetActive (false);
			} 
			else 
			{
				word.SetActive (true);
			}
		}

		if (treeslot_1 == null)
		{
			ranclone (Random.Range (1, 5));
			tree_cn.transform.position = treepos_1.transform.position;
			tree_cn.transform.SetParent (this.transform);
			tree_cn.transform.localScale = new Vector3 (tree_cn.transform.localScale.x*5f,tree_cn.transform.localScale.y*5f,tree_cn.transform.localScale.z*5f);
			tree_cn.transform.localEulerAngles = new Vector3 (45,0,30);
			treeslot_1 = tree_cn;
		}
		if (treeslot_2 == null)
		{
			ranclone (Random.Range (1, 5));
			tree_cn.transform.position = treepos_2.transform.position;
			tree_cn.transform.SetParent (this.transform);
			tree_cn.transform.localScale = new Vector3 (tree_cn.transform.localScale.x*5f,tree_cn.transform.localScale.y*5f,tree_cn.transform.localScale.z*5f);
			tree_cn.transform.localEulerAngles = new Vector3 (45,0,10);
			treeslot_2 = tree_cn;
		}
		if (treeslot_3 == null)
		{
			ranclone (Random.Range (1, 5));
			tree_cn.transform.position = treepos_3.transform.position;
			tree_cn.transform.SetParent (this.transform);
			tree_cn.transform.localScale = new Vector3 (tree_cn.transform.localScale.x*5f,tree_cn.transform.localScale.y*5f,tree_cn.transform.localScale.z*5f);
			tree_cn.transform.localEulerAngles = new Vector3 (45,0,-10);
			treeslot_3 = tree_cn;
		}
		if (treeslot_4 == null)
		{
			ranclone (Random.Range (1, 5));
			tree_cn.transform.position = treepos_4.transform.position;
			tree_cn.transform.SetParent (this.transform);
			tree_cn.transform.localScale = new Vector3 (tree_cn.transform.localScale.x*5f,tree_cn.transform.localScale.y*5f,tree_cn.transform.localScale.z*5f);
			tree_cn.transform.localEulerAngles = new Vector3 (45,0,-30);
			treeslot_4 = tree_cn;
		}
	}
	void ranclone (int xx)
	{
		if (xx == 1)
		{
			tree_cn = Instantiate (tree_1);
		}
		if (xx == 2)
		{
			tree_cn = Instantiate (tree_2);
		}
		if (xx == 3)
		{
			tree_cn = Instantiate (tree_3);
		}
		if (xx == 4)
		{
			tree_cn = Instantiate (tree_4);
		}
	}
}
