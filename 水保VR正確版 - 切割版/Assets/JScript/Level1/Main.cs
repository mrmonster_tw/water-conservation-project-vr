﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//using HTC.UnityPlugin.Vive;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using WindowsInput;

public class Main : MonoBehaviour {

	public float score = 0;
	public GameObject tosc;
	public int sccheck = 0;

	public GameObject start_1;
	public GameObject start_2;
	public GameObject start_3;
	public Texture start_white;
	public Texture start_half;
	public Texture start_full;

	public Material mat;

	public float time = 120;
	private float time_1;
	public float totaltime = 120;
	public bool timestart;
	public GameObject timesum;
	public RawImage numslot_1;
	public RawImage numslot_2;
	public RawImage numslot_3;
	private float numslot_h;
	private float numslot_t;
	private float numslot_s;
	public Texture num_0;
	public Texture num_1;
	public Texture num_2;
	public Texture num_3;
	public Texture num_4;
	public Texture num_5;
	public Texture num_6;
	public Texture num_7;
	public Texture num_8;
	public Texture num_9;
	//public GameObject bloodbar;
	public GameObject allstars;

	public Scene sc;

	public GameObject starprti;
	Animator ani;

	public int recordcheck = 0;
	public Text per;

	void Start ()
	{
		ani = GetComponent<Animator> ();
		sc = SceneManager.GetActiveScene ();
		tosc = GameObject.FindGameObjectWithTag ("tosc");
	}
	void Update ()
	{
		if (timesum.activeSelf && recordcheck == 0)
		{
			InputSimulator.SimulateKeyPress(VirtualKeyCode.F2);
			StartCoroutine (record());
			recordcheck = 1;
		}
		if (time <= 0 && sc.name == "1")
		{
			tosc.GetComponent<AudioSource> ().enabled = false;
			timesum.SetActive (false);
			per.enabled = false;
			allstars.SetActive (false);
			allstars.transform.localScale = new Vector3 (3, 3, 3);
			allstars.transform.localPosition = new Vector3 (0, -50, 0);
			ani.SetBool ("clear",true);
			StartCoroutine (loadscene("2"));
		}
		if (time <= 0 && sc.name == "2")
		{
			tosc.GetComponent<AudioSource> ().enabled = false;
			timesum.SetActive (false);
			per.enabled = false;
			allstars.SetActive (false);
			allstars.transform.localScale = new Vector3 (3, 3, 3);
			allstars.transform.localPosition = new Vector3 (0, -50, 0);
			ani.SetBool ("clear",true);
			StartCoroutine (loadscene("3"));
		}
		if (time <= 0 && sc.name == "3")
		{
			tosc.GetComponent<AudioSource> ().enabled = false;
			timesum.SetActive (false);
			per.enabled = false;
			allstars.SetActive (false);
			allstars.transform.localScale = new Vector3 (3, 3, 3);
			allstars.transform.localPosition = new Vector3 (0, -50, 0);
			ani.SetBool ("clear",true);
			StartCoroutine (loadscene("4"));
		}

//		bloodbar.transform.localScale = new Vector3 (time / totaltime, 1, 1);
		if (timestart && time > 0)
		{
			time = time - 1 * Time.deltaTime;
		}
		if (timestart && time > 10)
		{
			if (tosc.GetComponent<AudioSource>().volume < 0.3f)
			{
				tosc.GetComponent<AudioSource> ().enabled = true;
				tosc.GetComponent<AudioSource> ().volume += 0.05f*Time.deltaTime;
			}
		}
		if (timestart && time <= 10)
		{
			tosc.GetComponent<AudioSource> ().volume -= 0.05f*Time.deltaTime;
		}
		if (timestart && time <= 0)
		{
			time = 0;
		}
		time_1 = Mathf.Floor (time);
		numslot_h = Mathf.Floor(time_1 / 100);
		numslot_t = Mathf.Floor((time_1 - numslot_h*100)/10);
		numslot_s = time_1 - numslot_h*100 - numslot_t*10;
		number (numslot_h,numslot_1);
		number (numslot_t,numslot_2);
		number (numslot_s,numslot_3);
		if (numslot_h <= 0)
		{
			numslot_1.enabled = false;
			numslot_2.transform.localPosition = new Vector3 (-15,150,0);
			numslot_3.transform.localPosition = new Vector3 (15,150,0);
		}
		if (numslot_t <= 0 && numslot_1.enabled == false)
		{
			numslot_2.enabled = false;
			numslot_3.transform.localPosition = new Vector3 (0,150,0);
		}
		if (time < totaltime/2 && time > totaltime/4)
		{
			if (check == 0)
			{
				StartCoroutine (timebarchg_y());
			}
		}
		if (time <= totaltime/4)
		{
			if (check == 0)
			{
				StartCoroutine (timebarchg_r());
			}
		}

		if (score > 3)
		{
			per.enabled = true;
			per.text = "15%";
			start_1.GetComponent<RawImage> ().texture = start_half;
			if (sccheck == 0)
			{
				tosc.GetComponent<TotalScore> ().TotalSc = tosc.GetComponent<TotalScore> ().TotalSc + 0.5f;
				GameObject starparti_cn = Instantiate (starprti);
				starparti_cn.transform.SetParent (start_1.transform);
				starparti_cn.transform.localPosition = new Vector3 (0, 0, 0);
				Destroy (starparti_cn,2f);
				sccheck = 1;
			}
		}
		if (score > 6)
		{
			per.text = "30%";
			start_1.GetComponent<RawImage> ().texture = start_full;
			if (sccheck == 1)
			{
				tosc.GetComponent<TotalScore> ().TotalSc = tosc.GetComponent<TotalScore> ().TotalSc + 0.5f;
				GameObject starparti_cn = Instantiate (starprti);
				starparti_cn.transform.SetParent (start_1.transform);
				starparti_cn.transform.localPosition = new Vector3 (0, 0, 0);
				Destroy (starparti_cn,2f);
				sccheck = 2;
			}
		}
		if (score > 9) 
		{
			per.text = "45%";
			start_2.GetComponent<RawImage> ().texture = start_half;
			if (sccheck == 2) 
			{
				tosc.GetComponent<TotalScore> ().TotalSc = tosc.GetComponent<TotalScore> ().TotalSc + 0.5f;
				GameObject starparti_cn = Instantiate (starprti);
				starparti_cn.transform.SetParent (start_2.transform);
				starparti_cn.transform.localPosition = new Vector3 (0, 0, 0);
				Destroy (starparti_cn,2f);
				sccheck = 3;
			}
		}
		if (score > 12)
		{
			per.text = "60%";
			start_2.GetComponent<RawImage> ().texture = start_full;
			if (sccheck == 3)
			{
				tosc.GetComponent<TotalScore> ().TotalSc = tosc.GetComponent<TotalScore> ().TotalSc + 0.5f;
				GameObject starparti_cn = Instantiate (starprti);
				starparti_cn.transform.SetParent (start_2.transform);
				starparti_cn.transform.localPosition = new Vector3 (0, 0, 0);
				Destroy (starparti_cn,2f);
				sccheck = 4;
			}
		}
		if (score > 15)
		{
			per.text = "75%";
			start_3.GetComponent<RawImage> ().texture = start_half;
			if (sccheck == 4)
			{
				tosc.GetComponent<TotalScore> ().TotalSc = tosc.GetComponent<TotalScore> ().TotalSc + 0.5f;
				GameObject starparti_cn = Instantiate (starprti);
				starparti_cn.transform.SetParent (start_3.transform);
				starparti_cn.transform.localPosition = new Vector3 (0, 0, 0);
				Destroy (starparti_cn,2f);
				sccheck = 5;
			}
		}
		if (score > 18)
		{
			per.text = "100%";
			start_3.GetComponent<RawImage> ().texture = start_full;
			timestart = false;
			time = 0;
			if (sccheck == 5)
			{
				tosc.GetComponent<TotalScore> ().TotalSc = tosc.GetComponent<TotalScore> ().TotalSc + 0.5f;
				GameObject starparti_cn = Instantiate (starprti);
				starparti_cn.transform.SetParent (start_3.transform);
				starparti_cn.transform.localPosition = new Vector3 (0, 0, 0);
				Destroy (starparti_cn,2f);
				sccheck = 6;
			}
		}
	}
	private IEnumerator loadscene(string xx)
	{
		yield return new WaitForSeconds (7);
		SceneManager.LoadScene (xx);
	}
	int check = 0;
	private IEnumerator timebarchg_y()
	{
		check = 1;
		numslot_1.color = new Color (255,255,0);
		numslot_2.color = new Color (255,255,0);
		numslot_3.color = new Color (255,255,0);
		yield return new WaitForSeconds (1);
		numslot_1.color = new Color (255,255,255);
		numslot_2.color = new Color (255,255,255);
		numslot_3.color = new Color (255,255,255);
		yield return new WaitForSeconds (1);
		check = 0;
	}
	private IEnumerator timebarchg_r()
	{
		check = 1;
		numslot_1.color = new Color (255,0,0);
		numslot_2.color = new Color (255,0,0);
		numslot_3.color = new Color (255,0,0);
		yield return new WaitForSeconds (0.5f);
		numslot_1.color = new Color (255,255,255);
		numslot_2.color = new Color (255,255,255);
		numslot_3.color = new Color (255,255,255);
		yield return new WaitForSeconds (0.5f);
		check = 0;
	}
	private IEnumerator record()
	{
		yield return new WaitForSeconds (20);
		InputSimulator.SimulateKeyPress(VirtualKeyCode.F2);
	}
	public void number (float xx,RawImage zz)
	{
		if (xx == 0)
		{
			zz.texture = num_0;
		}
		if (xx == 1)
		{
			zz.texture = num_1;
		}
		if (xx == 2)
		{
			zz.texture = num_2;
		}
		if (xx == 3)
		{
			zz.texture = num_3;
		}
		if (xx == 4)
		{
			zz.texture = num_4;
		}
		if (xx == 5)
		{
			zz.texture = num_5;
		}
		if (xx == 6)
		{
			zz.texture = num_6;
		}
		if (xx == 7)
		{
			zz.texture = num_7;
		}
		if (xx == 8)
		{
			zz.texture = num_8;
		}
		if (xx == 9)
		{
			zz.texture = num_9;
		}
	}
}
