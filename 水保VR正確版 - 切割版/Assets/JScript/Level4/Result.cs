﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using WindowsInput;

public class Result : MonoBehaviour {

	public GameObject tosc;
	public GameObject mark_1;
	public GameObject mark_2;
	public GameObject mark_3;
	public GameObject light_1;
	public GameObject light_2;
	public GameObject light_3;
	public GameObject qrsum;
	public GameObject qr_pre1;
	public GameObject qr_pre2;
	public GameObject qr_pre3;

	public Texture word_1;
	public Texture word_2;
	public Texture word_3;

	public RawImage word;

	void Start ()
	{
		tosc = GameObject.FindGameObjectWithTag ("tosc");
		StartCoroutine (restart());

		GameObject qr_1_cn = Instantiate (qr_pre1);
		GameObject qr_2_cn = Instantiate (qr_pre2);
		GameObject qr_3_cn = Instantiate (qr_pre3);
		qr_1_cn.transform.SetParent (qrsum.transform);
		qr_2_cn.transform.SetParent (qrsum.transform);
		qr_3_cn.transform.SetParent (qrsum.transform);
		qr_1_cn.transform.localPosition = new Vector3 (-130,75,0);
		qr_2_cn.transform.localPosition = new Vector3 (0,75,0);
		qr_3_cn.transform.localPosition = new Vector3 (130,75,0);
		qr_1_cn.transform.localEulerAngles = new Vector3 (0,0,0);
		qr_2_cn.transform.localEulerAngles = new Vector3 (0,0,0);
		qr_3_cn.transform.localEulerAngles = new Vector3 (0,0,0);
		qr_1_cn.transform.localScale = new Vector3 (1,1,1);
		qr_2_cn.transform.localScale = new Vector3 (1,1,1);
		qr_3_cn.transform.localScale = new Vector3 (1,1,1);

		if (tosc.GetComponent<TotalScore> ().TotalSc < 9 && tosc.GetComponent<TotalScore> ().TotalSc >= 6) 
		{
			word.texture = word_2;
		}
		if (tosc.GetComponent<TotalScore> ().TotalSc < 6 && tosc.GetComponent<TotalScore> ().TotalSc >= 3) 
		{
			word.texture = word_2;
		}
		if (tosc.GetComponent<TotalScore> ().TotalSc < 3 && tosc.GetComponent<TotalScore> ().TotalSc >= 0) 
		{
			word.texture = word_3;
		}
		if (tosc.GetComponent<TotalScore> ().TotalSc >= 9) 
		{
			word.texture = word_1;
		}
	}
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Return) && tosc.GetComponent<TotalScore> ().TotalSc == 0)
		{
			SceneManager.LoadSceneAsync ("1");
		}
	}
	private IEnumerator restart ()
	{
		yield return new WaitForSeconds (8);
		mark_1.SetActive (true);
		mark_2.SetActive (true);
		mark_3.SetActive (true);
		word.enabled = true;
		yield return new WaitForSeconds (2);
		if (tosc.GetComponent<TotalScore> ().TotalSc >= 3)
		{
			mark_1.GetComponent<RawImage> ().color = new Color (255,255,255);
			light_1.SetActive (true);
		}
		yield return new WaitForSeconds (1);
		if (tosc.GetComponent<TotalScore> ().TotalSc >= 6) 
		{
			mark_2.GetComponent<RawImage> ().color = new Color (255,255,255);
			light_2.SetActive (true);
		}
		yield return new WaitForSeconds (1);
		if (tosc.GetComponent<TotalScore> ().TotalSc >= 9) 
		{
			mark_3.GetComponent<RawImage> ().color = new Color (255,255,255);
			light_3.SetActive (true);
		}
		yield return new WaitForSeconds (1);
		qrsum.SetActive (true);
		tosc.GetComponent<TotalScore> ().TotalSc = 0;
		tosc.GetComponent<QRcodeCreator> ().lv1_b = 0;
		tosc.GetComponent<QRcodeCreator> ().lv2_b = 0;
		tosc.GetComponent<QRcodeCreator> ().lv3_b = 0;
	}
}
