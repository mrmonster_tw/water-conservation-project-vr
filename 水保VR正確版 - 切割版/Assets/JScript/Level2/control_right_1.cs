﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//using HTC.UnityPlugin.Vive;

public class control_right_1 : MonoBehaviour {

	private SteamVR_TrackedObject trackObj;//控制器

	public GameObject syringe;
	public Animator syrani;

	public int check = 5;
	public bool waitbo = false;
	public GameObject flow;

	private SteamVR_Controller.Device controller
	{
		get
		{
			return SteamVR_Controller.Input((int)trackObj.index);
		}
	}

	public bool trig;
	public GameObject hint;
	public GameObject hint_1;
	public GameObject resyn;
	public GameObject timebar;
	public AudioSource reload;
	public AudioSource reloadmidi;


	void Start ()
	{
		trackObj = GetComponent<SteamVR_TrackedObject>();//取得控制器腳本物件

		syrani = syringe.GetComponent<Animator> ();
	}
	void Update ()
	{
		if (timebar.activeSelf)
		{
			syringe.SetActive (true);
		}
		if (check == 0 && timebar.activeSelf)
		{
			hint.SetActive (true);
			hint_1.SetActive (true);
			reload.enabled = true;
			resyn.SetActive (true);
		}
		else
		{
			hint.SetActive (false);
			hint_1.SetActive (false);
			reload.enabled = false;
			resyn.SetActive (false);
		}

		if (controller.GetPressDown (SteamVR_Controller.ButtonMask.Trigger) && waitbo == false) 
		{
			trig = true;
			if (check == 0)
			{
				reload.enabled = true;
			}
			if (check == 1)
			{
				reloadmidi.enabled = false;
				flow_fn ();
				syrani.SetBool ("fill",false);
				syrani.SetTrigger ("push");
				check = 0;
			}
			if (check == 2)
			{
				reloadmidi.enabled = false;
				flow_fn ();
				syrani.SetBool ("fill",false);
				syrani.SetTrigger ("push");
				check = 1;
			}
			if (check == 3)
			{
				reloadmidi.enabled = false;
				flow_fn ();
				syrani.SetBool ("fill",false);
				syrani.SetTrigger ("push");
				check = 2;
			}
			if (check == 4)
			{
				reloadmidi.enabled = false;
				flow_fn ();
				syrani.SetBool ("fill",false);
				syrani.SetTrigger ("push");
				check = 3;
			}
			waitbo = true;
			StartCoroutine (waitcheck());
		}
		if (controller.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) 
		{
			trig = false;
		}

	}
	private IEnumerator waitcheck()
	{
		yield return new WaitForSeconds (1f);
		waitbo = false;
	}
	private IEnumerator sywait()
	{
		yield return new WaitForSeconds (1f);
		check = 5;
	}
	public void flow_fn ()
	{
		GameObject flow_cn = Instantiate (flow);
		flow_cn.transform.SetParent (syringe.transform);
		flow_cn.transform.localPosition = new Vector3 (0f,-0.353f,0f);
		flow_cn.transform.localEulerAngles = new Vector3 (90,0,0);
		flow_cn.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
		Destroy (flow_cn, 1.5f);
	}
}