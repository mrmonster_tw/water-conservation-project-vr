﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tree : MonoBehaviour {

	public Material chgmat;
	public GameObject ctrl;
	public GameObject main;
	public int done = 0;

	public Material rim_mat;
	public Material org_mat;

	void Start ()
	{
		ctrl.GetComponent<control_right_1> ();
	}
	void Update ()
	{
		
	}
	void OnTriggerStay(Collider other)
	{
		if (other.tag == "syringe" && done == 0)
		{
			GetComponent<MeshRenderer> ().material = rim_mat;
			if (ctrl.GetComponent<control_right_1> ().trig && ctrl.GetComponent<control_right_1> ().check != 5)
			{
				this.GetComponent<MeshRenderer> ().material = chgmat;
				main.GetComponent<Main> ().score = main.GetComponent<Main> ().score + 0.7f;
				done = 1;
			}
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "syringe" && done == 0)
		{
			GetComponent<MeshRenderer> ().material = org_mat;
		}
	}
}
